<?php
$csv_file = 'data.csv';
if (isset($_POST['data'])) {
    ksort($_POST['data']);
    $handle = fopen($csv_file, "w");
    foreach ($_POST['data'] as $item) {
        ksort($item);
        fputcsv($handle, $item, ";");
    }
    fclose($handle);
    header("Location: " . $_SERVER["REQUEST_URI"]);
    exit;
}
$list = [];
if (file_exists($csv_file)) {
    if (($handle = fopen($csv_file, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ";")) !== FALSE) {
            $list[] = $data;
        }
        fclose($handle);
    }
}
?><!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>PHP Practice 2</title>
</head>
<body>
<form action="" method="POST">
    <div class="container">
        <h1>PHP Practice 2: Доска объявлений</h1>
        <div class="d-flex justify-content-center align-content-center flex-wrap">
            <div class="col-sm-6 col-md-4 p-2">
                <div class="card">
                    <div class="card-header">
                        Добавить новое объявление
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="email-field">email</label>
                            <input type="text" class="form-control" id="email-field" placeholder="email"
                                   name="data[0][0]"/>
                        </div>
                        <div class="form-group">
                            <label for="description-field">текст объявления</label>
                            <textarea class="form-control" id="description-field" rows="3"
                                      placeholder="текст объявления" name="data[0][3]"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="title-field">заголовок объявления</label>
                            <input type="text" class="form-control" id="title-field" placeholder="заголовок объявления"
                                   name="data[0][2]"/>
                        </div>
                        <div class="form-group">
                            <label for="category-field">категория объявления</label>
                            <select class="form-control" id="category-field" name="data[0][1]">
                                <option>Продажа</option>
                                <option>Вакансия</option>
                                <option>Обмен</option>
                                <option>Другое</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Создать</button>
                    </div>
                </div>
            </div>
            <?php foreach ($list as $i => $item): ?>
                <div class="col-sm-6 col-md-4 p-2">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="email-field<?= $i ?>">email</label>
                                <input type="text" class="form-control" readonly id="email-field<?= $i ?>"
                                       value="<?= htmlspecialchars($item[0], ENT_QUOTES) ?>"
                                       name="data[<?= $i + 1 ?>][0]"/>
                            </div>
                            <div class="form-group">
                                <label for="description-field<?= $i ?>">текст объявления</label>
                                <textarea class="form-control" readonly id="description-field<?= $i ?>" rows="6"
                                          placeholder="текст объявления"
                                          name="data[<?= $i + 1 ?>][3]"><?= htmlspecialchars($item[3],
                                        ENT_QUOTES) ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="title-field<?= $i ?>">заголовок объявления</label>
                                <input type="text" class="form-control" readonly id="title-field<?= $i ?>"
                                       value="<?= htmlspecialchars($item[2], ENT_QUOTES) ?>"
                                       name="data[<?= $i + 1 ?>][2]"/>
                            </div>
                            <div class="form-group">
                                <label for="category-field<?= $i ?>">категория объявления</label>
                                <input type="text" class="form-control" readonly id="category-field<?= $i ?>"
                                       value="<?= htmlspecialchars($item[1], ENT_QUOTES) ?>"
                                       name="data[<?= $i + 1 ?>][1]"/>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</form>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>
